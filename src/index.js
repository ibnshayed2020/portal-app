import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

//css
import "antd/dist/antd.css";
import "./assests/scss/styles.scss";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

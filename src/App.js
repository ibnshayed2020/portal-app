import React from 'react';
import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    UploadOutlined,
} from '@ant-design/icons';
import AsideLeft from "./layouts/AsideLeft";
import NavHeader from "./layouts/NavHeader";
import PageContent from "./layouts/PageContent";

const { Header, Sider, Content } = Layout;

class App extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <Layout>
                <AsideLeft/>
                <Layout className="site-layout">
                    <NavHeader/>
                    <PageContent/>
                </Layout>
            </Layout>
        );
    }
}

export default App;
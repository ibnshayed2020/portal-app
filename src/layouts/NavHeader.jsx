import React from 'react';

import {Layout} from 'antd';
import {MenuFoldOutlined, MenuUnfoldOutlined,} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

const NavHeader = ({collapsed,toggle}) => {
    return (
        <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: toggle,
            })}
        </Header>
    );
};

export default NavHeader;

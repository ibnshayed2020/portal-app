import React, {useState} from 'react';

import {Layout} from 'antd';

import AsideLeft from "./AsideLeft";
import NavHeader from "./NavHeader";
import PageContent from "./PageContent";


const { Header, Sider} = Layout;

const Dashboard = () => {

    const [collapsed, setcollapsed] = useState(false);

    
      const toggle = () => {
        setcollapsed(!collapsed)
      };
    

    return (
        <Layout>
            <AsideLeft collapsed={collapsed}/>
            <Layout>
                <NavHeader collapsed={collapsed} toggle={toggle}/>
                <PageContent/>
            </Layout>
        </Layout>
    )
}

export default Dashboard;
